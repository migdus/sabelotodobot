require('dotenv').config();
const Discord = require('discord.js');
const tallador = require('./tallador.js');
const client = new Discord.Client();
const p = require('./puntuacion.js');
let puntuacion = new p.Puntuacion();

/**
 * Array de textos para parseo de mensajes
 */
const nadieText = ["nadie", "NADIE", "Nadie", "NADIES", "NAIDES"];
const reiniciarText = ["!LIMPIAR PUNTAJES"];

const mostrarPuntajes = msg => {
    if (["!puntaje", "!puntajes"].includes(msg.content.toLowerCase())) {
        escribirPuntos(msg);
    }
};

const escribirPuntos = msg => {
    if (puntuacion.hayPuntos()) {
        msg.reply("Los puntajes de la sesión:\n" + puntuacion.mostrar());
    } else {
        msg.reply("no hay puntos para mostrar");
    }
};

const limpiarPuntajes = msg => {
    const esAdminInterino = msg.member.roles.find(rol => rol.name == 'administrador-interino');
    const esAdmin = msg.member.roles.find(rol => rol.name == 'Admin');
    if (tallador.esTallador(msg) || esAdminInterino || esAdmin) {
        if (reiniciarText.indexOf(msg.content) > -1) {
            msg.reply("Se reiniciarán los puntajes.");
            escribirPuntos(msg);
            puntuacion.limpiar();
            msg.channel.send("¡Ha arrancado una nueva ronda!");
        }
    }
};

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
    mostrarPuntajes(msg);
    tallador.procesar(msg, puntuacion);
    limpiarPuntajes(msg);

});

client.on('guildMemberAdd', member => {
    member.send('¡Hola! ¡Bienvenido/a al Sabelotodo! Si no habías jugado antes, mira el mensaje anclado (pinned message) para que te enteres de las reglas. Utiliza el comando _!ayuda_ para ver para qué sirvo, o _!puntajes_ para consultar los puntajes. ');
});

client.login(process.env.BOT_TOKEN);