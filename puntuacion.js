function Puntuacion() {
    this.map = new Map();
}

function puntuar(map, jugador, adicion) {
    const actual = map.get(jugador);
    const nuevaPuntuacion = actual === undefined ? adicion : actual + adicion;
    map.set(jugador, nuevaPuntuacion);
    return nuevaPuntuacion;
};

Puntuacion.prototype.darPunto = function (jugador) {
    return puntuar(this.map, jugador, 1);
}

Puntuacion.prototype.darMedioPunto = function (jugador) {
    return puntuar(this.map, jugador, 0.5);
}

Puntuacion.prototype.limpiar = function () {
    this.map.clear();
}

Puntuacion.prototype.hayPuntos = function () {
    return this.map.size > 0;
}

function ordenarPorPuntaje(map) {
    let resultado = new Map();
    map.forEach((puntos, jugador, map) => {
        resultado.has(puntos) ? resultado.get(puntos).push(jugador) : resultado.set(puntos, [jugador]);
    });
    return resultado;
}

function formatoPuntaje(ordenado) {
    let resultado = '';
    ordenado.forEach((jugadores, puntos, ordenado) => {
        let acum = '';
        jugadores.forEach(x => acum = acum + x + ' ');
        resultado = resultado + puntos + ' pt → ' + acum + '\n';
    });
    return resultado;
}

Puntuacion.prototype.mostrar = function () {
    return formatoPuntaje(ordenarPorPuntaje(this.map));
}

module.exports.Puntuacion = Puntuacion;