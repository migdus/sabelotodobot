var assert = require('assert');
var sinon = require('sinon');
const t = require('../tallador.js');
const discord = require('discord.js');

describe('Tallador', function () {
    it('Usuario es tallador', function () {
        var msg = {
            member: {
                roles: [{ name: 'tallador' }]
            }
        };
        assert(t.esTallador(msg));
    });

    describe('Mostrar ayuda', function () {

        const prueba = comando => {
            var msg = new Message();
            var reply = sinon.stub(msg, "reply");
            t.ayuda(msg, '!ayuda')
            reply.restore();
            sinon.assert.calledWith(reply, t.uso);
        };

        it('con mensaje: !ayuda', () => {
            prueba('!ayuda');
        });

        it('con mensaje: !aYuda', () => {
            prueba('!aYuda');
        });

        it('con mensaje: !help', () => {
            prueba('!help');
        });

        it('con mensaje: !heLp', () => {
            prueba('!heLp');
        });
    });
});